import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';
import { baseUrl } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EmployeeeService {

  private url = `${baseUrl}/employee`;
  private fileUpload = `${baseUrl}/upload-file`;

  constructor(private httpClient: HttpClient) { }


  getEmployees() {
    return this.httpClient.get(this.url);
  }

  deleteEmployee(id: number) {
    return this.httpClient.delete(this.url + '/' + id);
  }

  createEmployee(employee: Employee) {
    return this.httpClient.post(this.url, employee);
  }

  showEmployee(id: number) {
    return this.httpClient.get(this.url + '/' + id);
  }

  updateEmployee(employee: Employee) {
    return this.httpClient.put(this.url, employee);
  }

  uploadFile(file: File) {
    const formData = new FormData();
    formData.append('image', file);
    formData.append("reportProgress", "true");

    return this.httpClient.post(this.fileUpload, formData);
  }
}
