export class Employee {


  constructor(public id: number, public name: string, public email: string, public jobTitle: string, public phone: string, public fileUrl: string, public imagePath: string) { }

}
