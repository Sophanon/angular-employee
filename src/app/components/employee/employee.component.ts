import { query } from '@angular/animations';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmployeeeService } from 'src/app/services/employee.service';
import { Employee } from '../../models/employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees: Employee[] = [
  ];
  employee: any = null;

  loadingEmployees: boolean = false;
  loadingImage: boolean = false;
  loadingForm: boolean = false;

  deleteStatus: boolean = false;

  errorMessage: string = '';
  errorStatus: boolean = false;



  constructor(private service: EmployeeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  public getEmployees()
  {
    this.loadingEmployees = true;
    this.service.getEmployees().subscribe(data => {
      this.employees = this.parseJson(data);
      this.loadingEmployees = false;
    });
  }

  public deleteEmployee(id: number)
  {
    this.service.deleteEmployee(id).subscribe(data => {
      let employee = this.employees.findIndex(e => e.id == id);
      if (employee != -1) {
        this.employees.splice(employee, 1);
        this.deleteStatus = true;
        setInterval(() => {
          this.deleteStatus = false;
        }, 2000);
      }
    });
  }

  public parseJson(data: any)
  {
      return JSON.parse(JSON.stringify(data)).data;
  }

  public showImage(imageUrl: string)
  {
      var previewImg = document.createElement("img");
      previewImg.setAttribute("src", imageUrl);
      previewImg.classList.add("img-thumbnail");
      previewImg.classList.add("w-100");
      var preview = document.getElementById("preview");
      if (preview) {
        preview.innerHTML = "";
      }
      preview?.appendChild(previewImg);
  }

  public onOpenModal(employee: Employee, mode: string) : void {
    const container = document.getElementById('listing-employee');
    const button = document.createElement("button");
    button.type = "button";
    button.style.display = "none";
    button.setAttribute("data-toggle", "modal");
    if (mode == "edit") {
      this.loadingForm = true;
      this.service.showEmployee(employee.id).subscribe(data => {
        this.employee = this.parseJson(data);
        if (employee.fileUrl) {
          this.showImage(employee.fileUrl);
        }
        this.loadingForm = false;
        button.setAttribute("data-target", "#employeeModal");
        if (container) {
          container.appendChild(button);
          button.click();
        }

      });
    }else if (mode == "delete") {
      button.setAttribute("data-target", "#deleteEmployeeModal");
    }
  }

  public onAddModal() : void {
    this.employee = null;
    this.showImage("");
    const container = document.getElementById('listing-employee');
    const button = document.createElement("button");
    button.type = "button";
    button.style.display = "none";
    button.setAttribute("data-toggle", "modal");
    button.setAttribute("data-target", "#employeeModal");
    if (container) {
      container.appendChild(button);
      button.click();
    }
  }

  public onAddEmployee(addForm: NgForm) : void {
      addForm.value.imagePath = (<HTMLInputElement>document.getElementById("imagePath")).value;
      this.service.createEmployee(addForm.value).subscribe(data => {
        addForm.reset();
        this.closeModal("employeeModal");
        this.getEmployees();
      }, (error: HttpErrorResponse) => {

        this.errorMessage = error.error.errormesasge;
        this.errorStatus = true;
        setInterval(() => {
          this.errorStatus = false;
          this.errorMessage = "";
        }, 10000);
        // addForm.reset();
      });
  }

  public onUpdateEmployee(updateForm: NgForm) : void {
    updateForm.value.imagePath = (<HTMLInputElement>document.getElementById("imagePath")).value;
    this.service.updateEmployee(updateForm.value).subscribe(data => {
      this.closeModal("employeeModal");
      this.getEmployees();
    }, (error: HttpErrorResponse) => {
        this.errorMessage = error.error.errormesasge;
        this.errorStatus = true;
        setInterval(() => {
          this.errorStatus = false;
          this.errorMessage = "";
        }, 10000);
      // updateForm.reset();
    });
  }

  public closeModal(id: string) : void {
    const container = document.getElementById('listing-employee');
    const button = document.createElement("button");
    button.type = "button";
    button.style.display = "none";
    button.setAttribute("data-toggle", "modal");
    button.setAttribute("data-target", "#"+id);
    if (container) {
      container.appendChild(button);
      button.click();
    }
  }


  public dragNdrop(event: any) {
    this.service.uploadFile(event.target.files[0]).subscribe(data => {
        let file = this.parseJson(data);
        document.getElementById("imagePath")!.setAttribute("value", file.filePath);
    });
    var fileName = URL.createObjectURL(event.target.files[0]);
    this.showImage(fileName);
  }

  public drag() {
    const uploadFile = document.getElementById('uploadFile');
    if (uploadFile) {
      uploadFile.className = 'draging dragBox';
    }
  }

  public drop() {
    const uploadFile = document.getElementById('uploadFile');
    if (uploadFile) {
      uploadFile.className = 'dragBox';
    }
  }

  public logout()
  {
    window.location.href = "http://localhost:8080/auth/realms/employee/protocol/openid-connect/logout";
  }

}
